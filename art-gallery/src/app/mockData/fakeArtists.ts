export const fakeArtists = [
    {
        id: 1,
        name: 'john doe',
        nationality: 'French', 
    },
    {
        id: 2,
        name: 'Matias Perez',
        nationality: 'Chilean', 
    },
    {
        id: 3,
        name: 'Ethan johnson',
        nationality: 'American', 
    },
    {
        id: 4,
        name: 'Sophie patel',
        nationality: 'Indian', 
    },
    {
        id: 5,
        name: 'Mateo Fernandez',
        nationality: 'Spanish', 
    },
    {
        id: 6,
        name: 'Isabella rossi',
        nationality: 'Italian', 
    },
    {
        id: 7,
        name: 'Emily nguyen',
        nationality: 'Vietnamese', 
    },
    {
        id: 8,
        name: 'Oliver Müller',
        nationality: 'German', 
    },
    {
        id: 9,
        name: 'Ava kiimi',
        nationality: 'South Korean', 
    },
];