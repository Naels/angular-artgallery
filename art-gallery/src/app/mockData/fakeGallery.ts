export const fakeGallery = [
    {
      imageUrl: 'assets/images/mock/gallery/1.jpg'
    },
    {
      imageUrl: 'assets/images/mock/gallery/2.jpg'
    },
    {
      imageUrl: 'assets/images/mock/gallery/3.jpg'
    },
    {
      imageUrl: 'assets/images/mock/gallery/4.jpg'
    },
    {
      imageUrl: 'assets/images/mock/gallery/5.jpg'
    },
  ];