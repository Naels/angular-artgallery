import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ArtistProfileComponent } from './pages/artist-profile/artist-profile.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'artist/:id', component: ArtistProfileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }