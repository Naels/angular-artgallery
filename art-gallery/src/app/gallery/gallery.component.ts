import { Component, OnInit } from '@angular/core';
import { GalleryApiServiceService } from '../service/gallery-api-service.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { fakeGallery } from '../mockData/fakeGallery';



@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  artWorkList: any

  constructor(private service: GalleryApiServiceService) { }

  ngOnInit(): void {
    this.getArtworkPresentation()
  }

  getArtworkPresentation() {
    this.service.getArtWorkPresentation().pipe(
      catchError(() => of(fakeGallery))
    ).subscribe((res) => {
      this.artWorkList = res;
    });
  } 
}
