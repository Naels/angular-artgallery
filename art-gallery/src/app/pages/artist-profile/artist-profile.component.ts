import { Component, OnInit } from '@angular/core';
import { GalleryApiServiceService } from '../../service/gallery-api-service.service';
import { ActivatedRoute } from '@angular/router';

import { catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs'
import { MessageService } from 'src/app/service/messageService';
import { nationalityToCountry } from 'src/app/reusable/nationalityToCountry';

@Component({
  selector: 'app-artist-profile',
  templateUrl: './artist-profile.component.html',
  styleUrls: ['./artist-profile.component.css'],
  providers: [MessageService]
})
export class ArtistProfileComponent implements OnInit {

  constructor(private service: GalleryApiServiceService, private router: ActivatedRoute, private messageService: MessageService) { }

  artistInfos         : any
  artworkByThisArtist : any
  artworks            : any
  errorMessage        : any
  presentationArtist  : any

  nationalityToCountry = nationalityToCountry 


  ngOnInit(): void {
    let getParamId = this.router.snapshot.paramMap.get('id')
    this.getArtistInfos(getParamId)

  }

  getArtistInfos(id: any) {
    this.service.getArtistById(id).pipe(
      switchMap((res) => {
        this.artistInfos = res
        this.presentationArtist = this.messageService.getMessage( res?.name, res?.artistStyleList[0],
                                                                  res?.artistStyleList[1],
                                                                  res?.artistStyleList[2], 
                                                                  nationalityToCountry[res?.nationality])
        return this.service.getArtworkByArtistId(res.id).pipe(
          catchError((error) => {
            this.errorMessage = `Error retrieving artowrks : ${error}`
            return of([])
          })
        )
      })
    ).subscribe(
      (res) => this.artworks = res,
      (error) => {this.errorMessage = error}
    )
  }





}
