export class MessageService {
    private messageTemplate: string = "Meet {artistName}, a talented artist specializing in {artType}, {artType1} and {artType2}, based in {location}.";



  
    constructor() { }
  
    getMessage(artistName: string, artType: string, artType1: string, artType2: string, location: string): string {
      let message = this.messageTemplate.replace("{artistName}", artistName)
                                        .replace("{artType}", artType)
                                        .replace("{artType1}", artType1)
                                        .replace("{artType2}", artType2)
                                        .replace("{location}",location);
      return message;
    }
  }