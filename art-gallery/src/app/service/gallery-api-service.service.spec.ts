import { TestBed } from '@angular/core/testing';

import { GalleryApiServiceService } from './gallery-api-service.service';

describe('GalleryApiServiceService', () => {
  let service: GalleryApiServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryApiServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
