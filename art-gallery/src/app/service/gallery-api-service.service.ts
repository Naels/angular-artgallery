import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GalleryApiServiceService {

  private baseUrl = "http://localhost:8080"

  constructor(private http: HttpClient) { }

  getArtists(): Observable<any> {
    return this.http.get<any[]>(`${this.baseUrl}/artists`);
  }

  getArtWorkPresentation(): Observable<any> {
    return this.http.get<any[]>(`${this.baseUrl}/artworks/random`);
  }

  getArtistById(id: any): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/artists/${id}`);
  }

  getArtworkByArtistId(id: any): Observable<any> {
    return this.http.get<any[]>(`${this.baseUrl}/artworks/artist/id/${id}`);
  }

  getArtworkByArtist(name: any): Observable<any> {
    return this.http.get<any[]>(`${this.baseUrl}/artworks/artist/${name}`);
  }
}
