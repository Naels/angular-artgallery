import { Component, OnInit } from '@angular/core';
import { GalleryApiServiceService } from '../service/gallery-api-service.service';

import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { fakeArtists } from '../mockData/fakeArtists';

import { nationalityDictionary } from '../reusable/nationalityDictionary';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service: GalleryApiServiceService){}

  allArtists: any

  nationalityDico = nationalityDictionary

  ngOnInit(): void {
    this.getArtists() 
  }

  getArtists(){
    this.service.getArtists().pipe(
      catchError(() => of(fakeArtists))
    ).subscribe((res) => {
      this.allArtists = res;
    });
  }

  removeSpaces(str: string): string {
    return str.replace(/\s/g, '');
  }

}
