-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: artworkdb
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artwork`
--

DROP TABLE IF EXISTS `artwork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artwork` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `artist` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `artist_id` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artwork`
--

LOCK TABLES `artwork` WRITE;
/*!40000 ALTER TABLE `artwork` DISABLE KEYS */;
INSERT INTO `artwork` VALUES (1,'John Doe','A stunning painting capturing the beauty of a sunset','https://cdn.pixabay.com/photo/2017/07/03/20/17/colorful-2468874_1280.jpg','Sunset Painting',1),(2,'John Doe','A funky portrait','https://www.befunky.com/images/prismic/8378f663-d028-48f1-87de-fc5f872aa948_hero-photo-to-art-5.jpg','Funky portrait',1),(3,'John Doe','Recycled art','https://blog.artsper.com/wp-content/uploads/2018/08/Vik-muniz-van-gogh-min-2.jpg','Recycled art',1),(4,'John Doe','A beautiful night walk night time','https://artfiles.alphacoders.com/681/68159.jpg','Night walk',1),(5,'John Doe','A beautiful politician portrait','https://pixopolitan.com/wp-content/uploads/2022/03/theme-pixopolitan-street-art.jpg','Pixopolitan',1),(6,'John Doe','A beautiful path into nature','https://naturalsciences.org/calendar/wp-content/uploads/2019/12/Karin_Neuvirth_640x480.jpg','Nature way',1),(7,'Matias Perez','A beautiful portait of a women with face tatoo','https://static-secure.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/2/5/1391615935006/Mural-cerro-alegre-014.jpg','Face tatoo',2),(8,'Matias Perez','Faces painting','https://media.artsper.com/artwork/1604298_1_m.jpg','Fantastically Varia',2),(9,'Matias Perez','Guernica In Color','https://images.fineartamerica.com/images/artworkimages/mediumlarge/1/guernica-in-color-leon-keay.jpg','Guernica In Color',2),(10,'Matias Perez','Swiss light mountain snow','https://sothebys-com.brightspotcdn.com/02/e5/8380915f471a984f1e0de5339f22/paintings-swiss-consign.jpeg','Light Mount',2),(11,'Matias Perez','A color ful eye full of painting','https://img.freepik.com/photos-gratuite/portrait-oeil-abstrait-elegance-jeunes-femmes-generee-par-ia_188544-9712.jpg?q=10&h=200','Color full eye',2),(12,'Matias Perez','Color full faces full of painting','https://www.shutterstock.com/image-illustration/modern-illustration-linocut-style-surreal-260nw-1913052853.jpg','Faces',2),(13,'Ethan Johnson','Reflective Mountain','https://img.freepik.com/free-photo/mountain-landscape-reflects-autumn-colors-tranquil-water-generative-ai_188544-12861.jpg','Reflective Mountain',3),(14,'Ethan Johnson','Tree','https://img.freepik.com/photos-gratuite/nature-abstraite-peinte-fond-feuilles-automne-aquarelle-generee-par-ia_188544-9806.jpg','Tree',3),(15,'Ethan Johnson','Forest','https://www.cof.ens.fr/bda/wp-content/uploads/2016/02/art-auction.jpg','Forest',3),(16,'Ethan Johnson','Jungle','https://dza2a2ql7zktf.cloudfront.net/binaries-cdn/dqzqcuqf9/image/fetch/ar_16:10,q_auto:best,dpr_3.0,c_fill,f_auto,w_380/https://d2u3kfwd92fzu7.cloudfront.net/asset/cms/Art_Basel_Hong_Kong_2023-2.jpg','Jungle',3),(17,'Ethan Johnson','Mandala','https://aaf-preprod.s3.eu-west-1.amazonaws.com/uploads/2022/05/16062057/KOG_Gabriella-Possum-Nungurrayi_Grandmothers-Country-GPNU212268_2021_93x141_5500.png','Mandala',3),(18,'Ethan Johnson','Natural','https://w0.peakpx.com/wallpaper/330/433/HD-wallpaper-beautiful-nature-flowers-beauty-art-nature-paintings.jpg','Natural',3),(19,'Sophie Patel','Autumn','https://img.freepik.com/photos-premium/paysage-nature-fantastique-colore-art-numerique-paysages-banniere-site-web-dynamique-generes-par-ai_467123-10261.jpg','Autumn',4),(20,'Sophie Patel','Rain','https://img.freepik.com/photos-premium/paysage-nature-fantastique-colore-art-numerique-paysages-banniere-site-web-dynamique-generes-par-ai_467123-10401.jpg','Rain',4),(21,'Sophie Patel','City','https://img.freepik.com/photos-premium/paysage-nature-fantastique-colore-art-numerique-paysages-banniere-site-web-dynamique-generes-par-ai_467123-11003.jpg','City',4),(22,'Sophie Patel','Cars in the street','https://img.freepik.com/photos-premium/paysage-nature-fantastique-colore-art-numerique-paysages-banniere-site-web-dynamique-generes-par-ai_467123-11038.jpg','Cars in the street',4),(23,'Sophie Patel','Rainbow','https://img.freepik.com/photos-premium/belle-conception-illustration-paysage-pour-art-mural-nature-banniere-ai-genere_467123-10335.jpg','Rainbow',4),(24,'Sophie Patel','Solo walk','https://img.freepik.com/photos-premium/belle-conception-illustration-paysage-pour-art-mural-nature-banniere-ai-genere_467123-11055.jpg','Solo walk',4),(25,'Mateo Fernandez','Red & Blue','https://wallpaperaccess.com/full/496089.jpg','Red & Blue',5),(26,'Mateo Fernandez','Nebula','https://images.hdqwalls.com/wallpapers/neblua-space-planet-art-4k-ye.jpg','Nebula',5),(27,'Mateo Fernandez','Traveler','https://w0.peakpx.com/wallpaper/181/64/HD-wallpaper-traveler-planet-space-art.jpg','Traveler',5),(28,'Mateo Fernandez','Land-space','https://w0.peakpx.com/wallpaper/610/719/HD-wallpaper-landscape-planets-stars-space-art.jpg','Land-space',5),(29,'Mateo Fernandez','Rings','https://rare-gallery.com/mocahbig/81477-saturn-planet-digital-universe-artist-artwork.jpg','Rings',5),(30,'Mateo Fernandez','Galaxy','https://artfiles.alphacoders.com/866/86648.jpg','Galaxy',5),(31,'Isabella Rossi','Future','https://www.michigandaily.com/wp-content/uploads/2022/01/online-AI-art-take-2.jpg','Future',6),(32,'Isabella Rossi','Metropolis','https://cdna.artstation.com/p/assets/images/images/020/936/156/large/julian-seifert-olszewski-city-shot-ful.jpg','Metropolis',6),(33,'Isabella Rossi','Thinking','https://onecms-res.cloudinary.com/image/upload/s--O2ieHjKj--/c_fill,g_auto,h_468,w_830/f_auto,q_auto/v1/mediacorp/cna/image/2023/02/27/istock-1357759108.jpg?itok=zKcg0hVU','Thinking',6),(34,'Emily Nguyen','Fly away','https://www.shutterstock.com/blog/wp-content/uploads/sites/5/2019/01/digital-illustration-guide.jpg?w=960&h=600&crop=1','Fly away',7),(35,'Emily Nguyen','thoughts','https://giaiphapvieclam.com/wp-content/uploads/2022/04/fq.png','thoughts',7),(36,'Emily Nguyen','Moon','https://img.freepik.com/premium-photo/night-scenery-showing-melting-moon-creates-glowing-river-digital-art-style-illustration-painting_37402-926.jpg','Moon',7),(37,'Oliver Müller','Land-space','https://w0.peakpx.com/wallpaper/610/719/HD-wallpaper-landscape-planets-stars-space-art.jpg','Land-space',8),(38,'Oliver Müller','Rings','https://rare-gallery.com/mocahbig/81477-saturn-planet-digital-universe-artist-artwork.jpg','Rings',8),(39,'Oliver Müller','Galaxy','https://artfiles.alphacoders.com/866/86648.jpg','Galaxy',8),(40,'Ava Kiimi','Run','https://i.pinimg.com/originals/a7/49/b7/a749b7343957257a5c3f35de74b10488.jpg','Run',9),(41,'Ava Kiimi','Ball','https://images.saatchiart.com/saatchi/1152735/art/6846051/5915681-HSC00001-6.jpg','Ball',9),(42,'Ava Kiimi','Race','https://w0.peakpx.com/wallpaper/524/130/HD-wallpaper-formula-1-ferrari-on-white-art-sports-car-beautiful-illustration-artwork-ferrari-cars-automobile-painting-auto-wide-screen-formula-1.jpg','Race',9),(43,'Santiago Hernandez','Misunderstanding','https://thevirtualinstructor.com/blog/wp-content/uploads/2013/08/understanding-abstract-art.jpg','Misunderstanding',10),(44,'Santiago Hernandez','Chaos','https://img.freepik.com/photos-premium/utilisation-filtre-anaglyphe-met-evidence-tons-bleu-orange-jaune-generative-ai_909576-54.jpg','Chaos',10),(45,'Santiago Hernandez','Mother','https://conceptoit.net/wp-content/uploads/2022/03/art-abstrait-2.jpg','Mother',10);
/*!40000 ALTER TABLE `artwork` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-27 12:03:38
