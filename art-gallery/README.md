# Art Gallery Online


## Description

Welcome to Art Gallery Online! This application serves as a digital platform to showcase artworks. The app utilizes a custom-built web service and a database, all developed from scratch, to provide a seamless and immersive art gallery experience.

Key Features

1. Artwork Display: Users can browse through a wide range of artwork. Each artwork is accompanied by informations, such as title, artist name.

2. Artist Profiles: The app showcases dedicated profiles for artists, featuring their artistic statements, and portfolios. Users can explore the diverse backgrounds and artistic journeys of the featured artists.


## Installation and Setup

    
1. Clone the repository.
2. run `npm i` to install dependencies
3. Create and configure the database to store artwork and artist information. You can find a folder `Dump-artgallery` that contains all the SQL commands needed just import it in your sql app (Mysql workbench for ex)
4. Launch the web service called `api` in my projects list.
5. Run the application locally or deploy it to a web server.


Technologies Used

1. Backend: JavaSpringBoot 
2. Frontend: Angular
3. Database: MySQL db
